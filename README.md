# Funktionentheorie

Hier ist Material zur Vorlesung Funktionentheorie an der Universität
Augsburg im Sommersemester 2018 zu finden.
[Übungsblätter und Skript](http://algebra-und-zahlentheorie.gitlab.io/funktionentheorie/)
zur Vorlesung stehen als pdf zum Download bereit.
